---
MorpheusModelID: M0006

authors: [A. Boutillon, D. Jahn, S. González-Tirado, J. Starruß, L. Brusch, N. B. David]
contributors: [D. Jahn]

published_model: original
hidden: true

title: "Guidance By Followers"
date: "2021-04-09T18:04:00+01:00"
lastmod: "2021-04-16T17:12:00+01:00"

tags:
- α-Catenin
- Animal Pole
- Axial Mesoderm
- Cadherin
- Cell Migration
- Collective Migration
- CPM
- Gastrulation
- Mechanotransduction
- Mesendodermal Polster
- Run and Tumble
- Zebrafish

#categories:
#- ...

#draft: true
---

## Reference

This model is used in the publication by Boutillon et al., under review.

