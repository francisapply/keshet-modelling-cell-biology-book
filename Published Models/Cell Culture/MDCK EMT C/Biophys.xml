<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour

SI Figure 12.

We thank Lutz Brusch for providing a basic cell sheet simulation that we modified and adapted to this project </Details>
        <Title>YREsheetNRAShapeFactorq</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size symbol="size" value="400, 100, 0"/>
            <BoundaryConditions>
                <Condition boundary="x" type="constant"/>
                <Condition boundary="-x" type="noflux"/>
                <Condition boundary="y" type="periodic"/>
                <Condition boundary="-y" type="periodic"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime symbol="stoptime" value="1500"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Analysis>
        <Gnuplotter decorate="true" time-step="50">
            <Terminal name="png"/>
            <Plot>
                <Cells value="q" max="5" min="3" flooding="true">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="lemonchiffon"/>
                            <Color value="0.05" color="light-blue"/>
                            <Color value="0.1" color="light-red"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <!--    <Disabled>
        <Plot title="Rac1">
            <Cells value="R" flooding="true">
                <ColorMap>
                    <Color value="0" color="blue"/>
                    <Color value="3" color="red"/>
                    <Color value="4" color="yellow"/>
                </ColorMap>
            </Cells>
        </Plot>
    </Disabled>
-->
            <!--    <Disabled>
        <Plot>
            <Cells value="E" min="0" max="2" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="plum"/>
                        <Color value="4" color="blue"/>
                        <Color value="7" color="cyan"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
            <!--    <Disabled>
        <Plot>
            <Cells value="d.abs" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="skyblue"/>
                        <Color value="0.1" color="violet"/>
                        <Color value="0.2" color="salmon"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
            <!--    <Disabled>
        <Plot>
            <Cells value="Cr" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="red"/>
                        <Color value="0" color="red"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
        </Gnuplotter>
        <Logger time-step="1.0">
            <Input>
                <Symbol symbol-ref="cell.center.x"/>
                <Symbol symbol-ref="cell.center.y"/>
                <Symbol symbol-ref="q"/>
                <Symbol symbol-ref="N_dividing"/>
            </Input>
            <Output>
                <TextOutput/>
            </Output>
            <Plots>
                <Plot time-step="-1">
                    <Style point-size="0.05" style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis maximum="size.x" minimum="0.0">
                        <Symbol symbol-ref="cell.center.x"/>
                    </X-axis>
                    <Y-axis maximum="stoptime" minimum="0">
                        <Symbol symbol-ref="time"/>
                    </Y-axis>
                    <Color-bar maximum="5" minimum="3" palette="default">
                        <Symbol symbol-ref="q"/>
                    </Color-bar>
                </Plot>
                <Plot>
                    <Style style="points"/>
                    <Terminal terminal="png"/>
                    <X-axis>
                        <Symbol symbol-ref="time"/>
                    </X-axis>
                    <Y-axis>
                        <Symbol symbol-ref="N_dividing"/>
                    </Y-axis>
                </Plot>
            </Plots>
        </Logger>
        <ModelGraph reduced="false" include-tags="#untagged" format="dot"/>
    </Analysis>
    <Global>
        <Constant symbol="ky" name="Basal rate of YAP activation" value="0.1"/>
        <Constant symbol="kye" name="E-cadherin-dependent rate of YAP deactivation" value="1.8"/>
        <Constant symbol="Dy" name="Inactivation rate of YAP" value="2"/>
        <Constant symbol="C" name="Initial activation rate of E-cadherin" value="0.9"/>
        <Constant symbol="ke" name="YAP-dependent rate of E-cadherin expression" value="0.9"/>
        <Constant symbol="K" name="Dissociation constant of YAP-WT1 transcriptional constant" value="1"/>
        <Constant symbol="De" name="Inactivation rate of E-cadherin" value="1"/>
        <Constant symbol="h" name="Hill coefficient for E-cadherin" value="3"/>
        <Constant symbol="kr" name="YAP-dependent rate of Rac1 expression" value="1"/>
        <Constant symbol="Kr" name="Michaelis-Menten-like constant for Rac1" value="0.5"/>
        <Constant symbol="Dr" name="Degradation rate of Rac1" value="0.5"/>
        <Constant symbol="n" name="Hill coefficient for Rac1" value="6"/>
        <Constant symbol="alphaR" name="Rac activation fraction" value="1"/>
        <Constant symbol="kyr" name="Rac1-dependent rate of YAP activation" value="1.8"/>
        <!--    <Disabled>
        <Constant symbol="A2" name="basal adhesion" value="2">
            <Annotation>E-cad blocking</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant symbol="A2" name="Max E-cadherin adhesion constant" value="12">
            <Annotation>Control</Annotation>
        </Constant>
        <Constant symbol="A3" name="E-cadherin half &quot;saturation&quot; " value="0.85"/>
        <Constant symbol="C1" name="basal migration" value="0.4"/>
        <Constant symbol="C2" name="Max Rac1 migration constant" value="4"/>
        <Constant symbol="C3" name="Rac1 half &quot;saturation&quot;" value="3"/>
        <Constant symbol="tlim" name="max time for initial leader cells to emerge" value="100"/>
        <Constant symbol="frac" name="fraction of neighbourhood Cr that the cell receives each time Cr spreads to it" value="0.2"/>
        <!--    <Disabled>
        <Constant symbol="Ytot" name="Total YAP (active (Y) + inactive)" value="0.5">
            <Annotation>KD</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant symbol="Ytot" name="Total YAP (active (Y) + inactive)" value="1.2">
            <Annotation>Control</Annotation>
        </Constant>
        <!--    <Disabled>
        <Constant symbol="Ytot" name="Total YAP (active (Y) + inactive)" value="2">
            <Annotation>OE</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant symbol="Rtot" name="Total Rac1 (active (R) + inactive)" value="5"/>
        <Constant symbol="shareprob" name="parameter that determines probability of Cr spread in each time step" value="0.02"/>
        <Variable symbol="E" name="E-cadherin" value="0.0"/>
        <Variable symbol="Cr" name="Basal Rac1 activation rate" value="0.0"/>
        <!--    <Disabled>
        <Variable symbol="x_edge2" value="0.0"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Mapper name="leftmost edge cell" time-step="1.0">
            <Input value="(M>0)*cell.center.x*(cell.center.x>10) + (M==0)*size.x + (M>0)*size.x*(cell.center.x&lt;=10)"/>
            <Output symbol-ref="x_edge2" mapping="minimum"/>
        </Mapper>
    </Disabled>
-->
        <Variable symbol="N_dividing" value="0.0">
            <Annotation>Counter for number of cells inside division zone, needed to saturate cell division rate.</Annotation>
        </Variable>
        <Constant symbol="Nmax_dividing" value="150">
            <Annotation>Saturation limit for cell division.</Annotation>
        </Constant>
    </Global>
    <CellTypes>
        <CellType class="medium" name="medium"/>
        <CellType class="biological" name="dividingcell">
            <VolumeConstraint strength="1" target="50"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint strength="1" mode="aspherity" target="1"/>
            <CellDivision division-plane="major">
                <Condition>(rand_uni(0,1)&lt;0.006*(exp(-time/300)+0.2)) * (cell.center.x&lt;40) * (N_dividing&lt;Nmax_dividing)</Condition>
                <Triggers/>
                <Annotation>Only cells in the first 40 pixels can divide, regardless of domain size. Before, this was size.x*0.1</Annotation>
            </CellDivision>
            <CellDeath name="delete cells near right domain edge">
                <Condition>(cell.center.x>0.99*size.x)</Condition>
            </CellDeath>
            <DirectedMotion strength="C1+C2*R/(C3+R)" direction="1, 0.0, 0.0" name="cell migration"/>
            <Property symbol="Y" name="YAP" value="0"/>
            <Property symbol="E" name="E-cadherin" value="10"/>
            <Property symbol="R" name="Rac1" value="0"/>
            <Property symbol="Cr" value="0.001"/>
            <Property symbol="dist" name="distance from right domain edge" value="0.0"/>
            <Property symbol="avspeed" name="Sum of instantaneous speeds each time step (times 100)" value="0.0"/>
            <Property symbol="truavspeed" name="Average Speed * 100" value="0.0"/>
            <Property symbol="avspeed2" name="Sum of instantaneous speeds over final 200 time steps (times 100)" value="0.0"/>
            <Property symbol="truavspeed2" name="average speed over final 200 time steps (times 100)" value="0.0"/>
            <Property symbol="M" name="Contact with medium" value="0.0"/>
            <Property symbol="neigh" name="number of neighbour cells" value="0.0"/>
            <Property symbol="av" name="average neighbourhood Cr" value="0.0"/>
            <PropertyVector symbol="d" name="speed" value="0.0, 0.0, 0.0"/>
            <NeighborhoodReporter>
                <Input scaling="length" value="cell.type == celltype.medium.id"/>
                <Output symbol-ref="M" mapping="sum"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input scaling="cell" value="cell.type == celltype.dividingcell.id"/>
                <Output symbol-ref="neigh" mapping="sum"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input scaling="cell" value="Cr"/>
                <Output symbol-ref="av" mapping="average"/>
            </NeighborhoodReporter>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="Y" name="Equation for YAP">
                    <Expression>(ky+kyr*R)*(Ytot-Y) - (kye*Y*E + Dy*Y)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="E" name="Equation for E-cadherin">
                    <Expression>C - ke*Y^h/(K^h+Y^h) - De*E</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="R" name="Equation for Rac1">
                    <Expression>alphaR*(Cr + kr*Y^n/(Kr^n+Y^n))*(Rtot-R) - Dr*R</Expression>
                </DiffEqn>
                <Rule symbol-ref="Cr" name="Equation for Cr spread; at each time step before tlim, cells in contact with the medium have a small chance of being endowed with high Cr; at each time step, each cell has a small chance of having its Cr be augmented by the average neighbourhood Cr (up to a max value); cells at the left edge of the domain retain low Cr">
                    <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*av*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>20)</Expression>
                </Rule>
                <Rule symbol-ref="dist" name="distance from right domain edge">
                    <Expression>size.x - cell.center.x</Expression>
                </Rule>
                <Rule symbol-ref="avspeed" name="Sum of instantaneous speeds each time step (times 100)">
                    <Expression>avspeed+d.abs*100</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed" name="average speed over the simulation (times 100)">
                    <Expression>avspeed/time</Expression>
                </Rule>
                <Rule symbol-ref="avspeed2" name="Sum of instantaneous speeds over final 100 time steps (times 100)">
                    <Expression>avspeed2 + d.abs*100*(time > stoptime-201)</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed2" name="average speed over 100 time steps (times 100)">
                    <Expression>avspeed2/200</Expression>
                </Rule>
                <Rule symbol-ref="q">
                    <Expression>cell.surface/sqrt(cell.volume)</Expression>
                </Rule>
                <!--    <Disabled>
        <Rule symbol-ref="q">
            <Expression>cell.volume</Expression>
        </Rule>
    </Disabled>
-->
            </System>
            <Property symbol="q" name="shape index" value="0.0"/>
            <Mapper name="N_dividing" time-step="1.0">
                <Input value="cell.center.x&lt;40"/>
                <Output symbol-ref="N_dividing" mapping="sum"/>
            </Mapper>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population size="1" name="Initialize cell sheet at left edge of the domain" type="dividingcell">
            <!--    <Disabled>
        <InitRectangle number-of-cells="70" mode="regular">
            <Dimensions size="size.x/100, size.y, size.z" origin="size.x/100, 0, 0"/>
        </InitRectangle>
    </Disabled>
-->
            <InitRectangle number-of-cells="70" mode="regular">
                <Dimensions size="4.0, size.y, 0.0" origin="0.0, 0.0, 0.0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction>
            <Contact type2="dividingcell" type1="dividingcell" value="30">
                <AddonAdhesion strength="5" adhesive="A2*E/(A3+E)" name="Adhesive strength"/>
            </Contact>
            <Contact type2="medium" type1="dividingcell" value="12"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>
