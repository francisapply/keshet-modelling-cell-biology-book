---
MorpheusModelID: M7121

title: "MDCK EMT A"

authors: [N. Mukhtar, E. N. Cytrynbaum, L. Edelstein-Keshet]
contributors: [E. N. Cytrynbaum]

# Under review
hidden: true
private: true

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume: 
#  issue: 
#  page: ""
#  year:
#  original_model: true

#categories:
#- DOI:...

#tags:
#- ...
---
<!--
## Introduction

…

## Description

…

## Results

…
-->

## Reference

This model is used in the publication by Mukhtar *et al.* under review.
