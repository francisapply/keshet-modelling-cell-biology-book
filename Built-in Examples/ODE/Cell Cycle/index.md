---
MorpheusModelID: M0001

authors: [J. E. Ferrell Jr., T. Y. Tsai, Q. Yang]

title: "ODE Model: Cell Cycle"
date: "2019-10-29T15:26:00+01:00"
lastmod: "2020-09-16T14:29:00+02:00"

aliases: ["/examples/ode-model-cell-cycle/"]

menu:
  Built-in Examples:
    parent: ODE
    weight: 10
weight: 20
---
## Introduction

This model is a simple three-species ODE model of the *Xenopus* embryonic cell cycle ([Ferrell *et al.*, 2011][ferrell-2011]). It exhibits sustained limit cycle oscillations.

![](cellcycle_singlecell.png "Time plots of ODE model of *Xenopus* embryonic cell cycle.")

## Description

One ```CellType``` is created that has three variables of ```Properties``` representing the concentrations of $\mathrm{APC}$, $\mathrm{Plk1}$, and $\mathrm{CDK1}$. These variables are coupled in a ```System``` of ```DiffEqn```s. 

In the ```System```, a number of ```Constants``` are defined, whose symbols are used in the ```DiffEqn```. Note that the equations are entered in simple plain text.

The ```System``` uses the ```runga-kutta``` (4th order) solver for the differential equations and speficies a particular time step (here $ht = 10^{-2}$) which is interpreted in global time steps. 

The global time is defined in the ```Time``` element and runs from ```StartTime``` to ```StopTime``` ($0$ to $25$). In this non-spatial model, ```Space``` defines a ```Lattice``` of size $(x,y,z)=(1,1,0)$.

Results are written to a file using the ```Analysis``` plugin ```Logger```. The ```Logger``` also visualizes the time plot to screen (in *interactive* mode) or to PNG files (in *local* mode).

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/70781290?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Things to try

- Change the dynamics by altering ```System/time-scaling```.

## Reference

>J. E. Ferrell Jr., T. Y. Tsai, Q. Yang: [Modeling the Cell Cycle: Why Do Certain Circuits Oscillate?][ferrell-2011]. *Cell* **144** (6): 874-885, 2011.


[ferrell-2011]: http://dx.doi.org/10.1016/j.cell.2011.03.006