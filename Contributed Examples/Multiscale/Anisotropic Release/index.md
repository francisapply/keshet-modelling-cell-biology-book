---
MorpheusModelID: M5482

contributors: [Roger, W. de Back, F. El-Hendi]

title: "Anisotropic release from cell"

tags:
- Contributed Examples
- 2D
- PDE
- Diffusion
- Cellular Potts Model
- CPM
- Field
- Field-Cell
- MembraneLattice
- Membrane
- MembraneProperty
- Multiscale
- Chemotaxis

# order
#weight: 20
---

>Membrane pattern allows anisotropic release of substance from cell

## Introduction

This example serves to demonstrate a multiscale model of a cell secreting a substance/molecule and propelling away from the secreted substance. Please also see the built-in docu at ```GUI/Documentation/MorpheuML/Space/MembraneLattice```, ```.../CellType/MembraneProperty``` and ```.../CellType/Chemotaxis```. This example was motivated by [Roger in the Morpheus user forum](https://groups.google.com/g/morpheus-users/c/tw37t651z-0)

## Description

A cell in Morpheus can act in an anisotropic manner by using a heterogeneously patterned ```MembraneProperty``` - here called ```m```. These are ```Properties``` with a spatial resolution (a 1D or 2D array, defined under ```Space/MembraneLattice```) that are mapped to the cell's membrane in polar coordinates in 2D or 3D, respectively. You can use this to represent cell polarity​, i.e. give directionality to processes. 

You first specify a ```Space / MembraneLattice``` and than define your ```CellType / MembraneProperty```. And you can use this as if it were a normal ```Property```, but one that is spatially heterogeneous along the cell membrane. In this example, ```m``` enters the ```Global/System``` for the secreted (and diffusing and degraded) substance ```c```.
You can then use the ```Chemotaxis``` plugin to simulate cell motility that is propelling away from the secreted substance. For that you simply need to specify the global  ```Field```, here "c" as ```field``` in the ```Chemotaxis plugin```. 
Moreover, ```CellType/Mapper``` would allow to couple ```m``` to scalar ```CellType/Property``` if needed. You may also add a separate ```CellType/System``` to dynamically change ```m```.

Movie visualising the secretion of a substance using MembraneLattice and propelling away from it:
![Movie visualising the secretion of a substance using MembraneLattice and propelling away from it.](anisotropic_release.mp4)

The panel shows the membrane concentration "m" of the substance (left) and the global field concentration "c" over time (right).


